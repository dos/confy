#!/bin/bash
set -x
set -e

REPOID="net.kirgroup"
APPID="net.kirgroup.confy"
GPGKEY=""
REMOTE=""
BASEDIR=""

. .makeflatpak.env

BUILD="$BASEDIR/build"
REPO="$BASEDIR/repo"

[ -d "$BUILD" ] && rm -fr "$BUILD"

flatpak-builder -v "$BUILD" "$APPID.devel.json"
flatpak build-export --gpg-sign="$GPGKEY" "$REPO" "$BUILD"
flatpak build-bundle --gpg-sign="$GPGKEY" "$REPO" "$BASEDIR/$APPID.flatpak" $APPID
#flatpak build-update-repo --title="Kirgroup" --comment="Develop packages from kirgroup.net" --icon="https://kirgroup.net/kirblack.png" "$REPO"

cat > "$BASEDIR/$APPID.flatpakref" <<EOF
[Flatpak Ref]
Name=$APPID
Branch=master
Title=$APPID Devel
Url=https://confy.kirgroup.net/repo/
RuntimeRepo=https://dl.flathub.org/repo/flathub.flatpakrepo
IsRuntime=false
GPGKey=`gpg --export "$GPGKEY" | base64 --wrap=0 `
EOF

cat > "$BASEDIR/$REPOID.flatpakrepo" <<EOF
[Flatpak Repo]
Title=Kirgroup
Url=https://confy.kirgroup.net/repo/
Homepage=https://confy.kirgroup.net/
Comment=Develop packages from kirgroup.net
Description=Develop packages from kirgroup.net
Icon=https://kirgroup.net/kirblack.png
GPGKey=`gpg --export "$GPGKEY" | base64 --wrap=0 `
EOF

rsync -v -e ssh "$BASEDIR/$REPOID"* "$REMOTE"
rsync -r -a -v -e ssh --delete "$REPO" "$REMOTE"/repo
