# self.py
#
# Copyright 2020 Fabio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from gi.repository import GLib
from gi.repository import Gtk
from gi.repository import GObject
from gi.repository import GdkPixbuf
from gi.repository import Gio
from gi.repository import Handy
from gi.repository import Notify

from .config import APP_NAME, APP_ID, APP_VERSION
from . import pages
from . import local
from . import models
from .preferences import PreferencesWindow
from .widgets import LoadingWidget, ErrorWidget, PlaceholderWidget
from .widgets import NotificationOverlay, LoadingOverlay
from .widgets import InputDialog, ConferenceEditDialog


class NetworkConnectionMonitor(GObject.Object):
    def __init__(self):
        super().__init__()
        self._isconnected = False
        self.nm = Gio.NetworkMonitor.get_default()
        self.nm.connect("network-changed", self.on_network_changed)
        self.on_network_changed()

    def on_network_changed(self, *args):
        _ic = self._isconnected
        self._isconnected = self.nm.get_connectivity() == Gio.NetworkConnectivity.FULL
        if (self._isconnected != _ic):
            #print("NetworkConnectionMonitor.isconnected", self._isconnected)
            self.notify("isconnected")

    @GObject.Property
    def isconnected(self):
        """The system can reach the global netowrk. Bool, read-only"""
        #print("NetworkConnectionMonitor.isconnected getter:", self._isconnected)
        return self._isconnected


class StartView(pages.BaseListPage):
    def __init__(self, window, **kwargs):
        self.window = window
        self.nm = NetworkConnectionMonitor()
        self.data = []

        super().__init__(**kwargs)

        self.get_style_context().add_class("start-view")
        self.get_style_context().add_class("clean-list")

        self.column.set_maximum_size(500)
        title_bar = Handy.HeaderBar(
            centering_policy = Handy.CenteringPolicy.STRICT,
            show_close_button = True,
            hexpand = True,
            title = APP_NAME
        )

        placeholder = PlaceholderWidget(
            APP_NAME,
            _("No conferences yet.") + "\n" + \
            _("Add a conference with the add button in title bar."))
        self.listbox.set_placeholder(placeholder)

        fetcher = local.open_menu(self.nm.props.isconnected)
        if fetcher is not None:
            self.window.loading.show(fetcher)
            fetcher.connect("done", self.update_list)
            fetcher.connect("error", lambda s,e: self.window.notification.show(_("<b>Error loading events list:</b>\n{}").format(e)))
        else:
            self.update_list()

        # app menu
        appmenumodel = Gio.Menu()
        appmenumodel.append(_("Preferences"), "win.show-preferences")
        appmenumodel.append(_("About {}").format(APP_NAME), "win.about")

        self.start_button = Gtk.Button.new_from_icon_name('open-menu-symbolic', 1)
        self.start_button.set_tooltip_text(_("Show window menu"))
        appmenu = Gtk.Popover.new_from_model(self.start_button, appmenumodel)
        self.start_button.connect('clicked',  lambda _: appmenu.popup())
        title_bar.pack_end(self.start_button)

        self.update_button = Gtk.Button.new_from_icon_name('view-refresh-symbolic', 1)
        self.update_button.set_tooltip_text(_("Refresh events list"))
        self.update_button.set_sensitive(self.nm.props.isconnected)
        self.update_button.connect('clicked', self.update_schedules)
        title_bar.pack_end(self.update_button)

        self.open_button = Gtk.Button.new_from_icon_name('list-add-symbolic', 1)
        self.open_button.set_tooltip_text(_("Add event from URL"))
        self.open_button.set_sensitive(self.nm.props.isconnected)
        self.open_button.connect('clicked', self.open_custom_url)
        title_bar.pack_start(self.open_button)

        # can't do this because: Warning: ../glib/gobject/gbinding.c:271: Unable to convert a value of type PyObject to a value of type gboolean
        #self.nm.bind_property("isconnected", update_action, "enabled", 0 )
        self.nm.connect("notify",self.on_networkconnectionmonitor_notify)

        window.set_titlebar(title_bar)
        title_bar.show_all()
        self.show_all()

    def on_networkconnectionmonitor_notify(self, *args):
        is_connected = self.nm.props.isconnected
        self.update_button.set_sensitive(is_connected)
        self.open_button.set_sensitive(is_connected)
        
    def update_schedules(self, *args):
        fetcher = local.update_menu()
        def _done(s, f):
            self.update_list()
            self.window.notification.show(_("<b>Events list updated</b>"))

        def _error(s, e):
            self.window.notification.show(_("<b>Error loading events list:</b>\n{}").format(e))

        self.window.loading.show(fetcher)
        fetcher.connect("done", _done)
        fetcher.connect("error", _error)
    
    def edit_meta(self, url):
        conf = models.Conference.by_url(url)
        if conf is None:
            return
        d = ConferenceEditDialog(self.window, conf)
        ret = d.run()
        if ret == ConferenceEditDialog.OK:
            conf = d.get_conf()
            local.update_user_menu(conf.to_json())
            with conf.get_meta() as m:
                m.title = conf.title
                m.start = conf.start
                m.end = conf.end
            self.update_list()
        d.destroy()
        return conf

    def open_custom_url(self, *args):
        def _cbk(meta):
            conf = self.edit_meta(meta.url)
            self.update_list()
            self.window.notification.show(_("<b>New event:</b>\n{}").format(conf.title))

        def _error(s, e):
            self.window.notification.show(_("<b>Error:</b>\n{}").format(e))

        dialog = InputDialog(self.window)
        res = dialog.run()
        if res == InputDialog.OK:
            url = dialog.get_text()
            try:
                fetcher = local.add_user_menu(url, _cbk)
                if fetcher is not None:
                    fetcher.connect("error", _error)
            except local.MenuItemAlreadyExistsException as e:
                # show existing obj
                conf = models.Conference()._init(e.obj)
                self.window.show_main(conf)
            except Exception as e:
                import traceback;  traceback.print_exc()
                self.window.notification.show(_("<b>Error:</b>\n{}").format(e))
            else:
                if fetcher is not None:
                    self.window.loading.show(fetcher)

        dialog.destroy()

    def get_objects(self):
        confs = self.data
        try:
            confs = models.Conference.all()
        except Exception as e:
            self.window.notification.show(_("<b>Error loading events list:</b>\n{}").format(e))
        return confs


    def group_by(self, obj):
        if obj.end < datetime.date.today():
            return _("Past")
        else:
            return _("Coming up")
        
    def build_row(self, obj):
        row = Handy.ActionRow(activatable=True, selectable=False)
        row.set_title(obj.title)
        return row

    def on_activate(self, listbox, actionrow):
        idx = actionrow.get_index()
        obj = self.data[idx]
        self.window.show_main(obj)


class MainView(Gtk.VBox):
    """
    window
        overlay: notification
        title_bar
            start_button
            update_button
            'squeezer'
                top_switcher
                title_label
        box (self)
            mainstack
                leaflet ("schedule")
                    main_menu
                    Gtk.Separator
                    stack <Handy.Deck>
                        [pages]
                starred ("starred")
                map ("map")
            bottom_switcher
    """
    def __init__(self, window, conf):
        super().__init__()
        
        title_bar = Handy.HeaderBar(
            centering_policy = Handy.CenteringPolicy.STRICT,
            show_close_button = True,
            hexpand = True
        )
        window.set_titlebar(title_bar)
        self.window = window
        self.conf = conf

        self.notifications = {} # keep track of desktop notifications

        self.nm = NetworkConnectionMonitor()


        # app menu, it's always usefull
        appmenumodel = Gio.Menu()
        if conf.user:
            appmenumodel.append(_("Edit info"), "win.edit-meta")
        appmenumodel.append(_("Return to events list"), "win.return-to-events")
        appmenumodel.append(_("Preferences"), "win.show-preferences")
        appmenumodel.append(_("About {}").format(APP_NAME), "win.about")

        self.start_button = Gtk.Button.new_from_icon_name('open-menu-symbolic', 1)
        self.start_button.set_tooltip_text(_("Show window menu"))

        appmenu = Gtk.Popover.new_from_model(self.start_button, appmenumodel)
        self.start_button.connect('clicked',  lambda _: appmenu.popup())
        title_bar.pack_end(self.start_button)



        self.url = conf.url
        # open db update should be triggered by network state and data age
        try:
            fetcher = local.openconf(conf, is_online=self.nm.props.isconnected)
        except Exception as e:
            e = ErrorWidget(_("Error loading event"), e)
            self.pack_start(e, True, True, 0)
            title_bar.show_all()
            self.show_all()
        else:
            if fetcher is not None:
                fetcher.connect("done", self.on_fetcher_done)
                fetcher.connect("error", self.on_fetcher_error)

                self.pack_start(LoadingWidget(fetcher), True, True, 0)
                title_bar.show_all()
                self.show_all()
            else:
                self.build_gui()

    def build_gui(self):
        title_bar = self.window.get_titlebar()

        # title bar buttons:
        self.update_button = Gtk.Button.new_from_icon_name('view-refresh-symbolic', 1)
        self.update_button.set_tooltip_text("Refresh event")
        self.update_button.set_sensitive(self.nm.props.isconnected)
        self.update_button.connect('clicked', self.update_event)
        title_bar.pack_end(self.update_button)

        self.nm.connect("notify",self.on_networkconnectionmonitor_notify)

        self.back_button = Gtk.Button.new_from_icon_name(
                'go-previous-symbolic', 1)
        self.back_button.connect('clicked', self.goBack)
        title_bar.pack_start(self.back_button)
        
        # mainstack
        
        self.mainstack = Gtk.Stack(
            transition_type=Gtk.StackTransitionType.NONE,
            vexpand_set=True,
            vexpand=True)
        
        self.leaflet = Handy.Leaflet(
            can_swipe_back=True,
            hhomogeneous_folded=True,
            hhomogeneous_unfolded=False)

        self.leaflet.connect('notify::folded', self.on_leaflet_notify)
        self.leaflet.connect('notify::visible-child', self.on_leaflet_notify)
        self.main_menu = pages.MainMenuPage(pageStack=self, conf=self.conf)

        self.stack_count = 1
        self.stack = Handy.Deck(
            can_swipe_back=True,
            can_swipe_forward=False)
        #self.stack.connect("notify::visible-child", self.on_stack_changed)
        self.stack.connect("notify::transition-running", self.on_stack_changed)
        
        # add main menu and stack to leaflet
        self.leaflet.add(self.main_menu)
        sep = Gtk.Separator()
        self.leaflet.add(sep)
        self.leaflet.child_set_property(sep, 'navigatable', False)
        self.leaflet.add(self.stack)
        
        # add first page to stack
        self.stack.add(pages.ConferencePage(self.conf))
        

        # main stack content

        self.mainstack.add_titled(self.leaflet, "schedule", _("Schedule"))
        self.mainstack.child_set_property(self.leaflet, "icon-name", "x-office-calendar-symbolic")
        
        self.starred = pages.StarredPage()
        self.starred._set_pagestack(self)
        self.mainstack.add_titled(self.starred, "starred", _("Starred"))
        self.mainstack.child_set_property(self.starred, "icon-name", "starred-symbolic")
        

        if len(self.conf.get_map_links()) > 0:
            self.map = pages.MapPage(self.conf)
            self.mainstack.add_titled(self.map, "map", _("Map"))
            self.mainstack.child_set_property(self.map, "icon-name", "mark-location-symbolic")

        # main stack switchers
        self.top_switcher = Handy.ViewSwitcher(
            policy=Handy.ViewSwitcherPolicy.WIDE,
            stack=self.mainstack)
        self.bottom_switcher = Handy.ViewSwitcherBar(
            stack=self.mainstack)
        
        #mainstack event
        self.mainstack.connect("notify::visible-child", self.on_mainstack_changed)
        
        squeezer = Handy.Squeezer()
        squeezer.add(self.top_switcher)
        title_label = Gtk.Label()
        #title_label.set_markup("<b>{}</b>".format(APP_NAME))
        squeezer.add(title_label)
        title_bar.set_custom_title(squeezer)
        squeezer.connect("notify::visible-child",self.on_headerbar_squeezer_notify)
        
        # main box contanier

        self.pack_start(self.mainstack, True, True, 0)
        self.pack_start(self.bottom_switcher, False, False, 0)
        
        title_bar.show_all()
        self.show_all()
        
        # event notification timer
        # every minute
        GLib.timeout_add_seconds(60, self.check_event_notification)
        self.check_event_notification()

        self.update_back_button()

        # set start mainstack page
        self.mainstack.set_visible_child(self.leaflet)

    def on_networkconnectionmonitor_notify(self, *args):
        is_connected = self.nm.props.isconnected
        self.update_button.set_sensitive(is_connected)

    def on_mainstack_changed(self, *args):
        self.update_back_button()
        if (self.mainstack.get_visible_child() == self.starred):
            self.starred.update_list()
        
    def on_headerbar_squeezer_notify(self, squeezer, event):
        child = squeezer.get_visible_child()
        self.bottom_switcher.set_reveal(child != self.top_switcher)
    
    def on_leaflet_notify(self, sender, prop):
        self.update_back_button()

    def on_fetcher_done(self, *args):
        self.foreach(lambda obj: obj.destroy())
        self.build_gui()

    def on_fetcher_error(self, s, e):
        self.foreach(lambda obj: obj.destroy())
        e = ErrorWidget(_("Error loading event"), e)
        self.pack_start(e, True, True, 0)
        e.show_all()

    def edit_meta(self):
        d = ConferenceEditDialog(
            self.window, self.conf, can_cancel=True, can_delete=True)
        ret = d.run()
        self.conf = d.get_conf()
        d.destroy()
        if ret == ConferenceEditDialog.OK:
            local.update_user_menu(self.conf.to_json())
            with self.conf.get_meta() as m:
                m.title = self.conf.title
                m.start = self.conf.start
                m.end = self.conf.end
            self._update_ui()
            self.window.notification.show(_("<b>Updated event:</b>\n{}").format(self.conf.title))
        if ret == ConferenceEditDialog.DELETE:
            local.delete_user_menu(self.conf.to_json())
            self.window.notification.show(_("<b>Deleted event:</b>\n{}").format(self.conf.title))
            # we could delete cache db here...
            # get back to conferences list
            self.window.show_start()

    def _update_ui(self):
        """update ui when database change. Reset view to ConferencePage"""
        self.main_menu.update()
        self.replaceWithPage(pages.ConferencePage(self.conf))

    def update_event(self, *args):
        """update cached event data"""
        def _done(*args):
            self.window.loading.close()
            self.window.notification.show(_("<b>Event schedule updated</b>"))
            self._update_ui()

        def _error(s, e):
            self.window.loading.close()
            self.window.notification.show(_("<b>Error:</b>\n{}").format(e))

        fetcher = local.updatedb(self.url)
        self.window.loading.show(fetcher)
        fetcher.connect("done", _done)
        fetcher.connect("error", _error)
        #TODO UPDATE GUI!!! ! !

    def check_event_notification(self, *args):
        nextup = models.Event.nextup(5)
        for e in nextup:
            self._send_desktop_notification(
                _("Next up: {}").format(e.title),
                _("at {} in {}").format(
                    e.start.strftime("%H:%M"),
                    e.room
                ),
                f"nextup-{e.id}")
            e.set_notified(True)
        return True # keep timer running

    def _send_desktop_notification(self, title, body, nid=None):
        if nid in self.notifications:
            return
        n = Notify.Notification.new(title, body, "net.kirgroup.confy")
        n.set_timeout(Notify.EXPIRES_NEVER)
        #n.add_action("view", "View", on_notification_action, nid)
        n.show()
        self.notifications[nid] = n

    """
    def _gio_send_desktop_notification(self, title, body, nid=None):
        n = Gio.Notification.new(title)
        n.set_body(body)
        n.set_icon(Gio.ThemedIcon.new("net.kirgroup.confy"))
        n.set_priority(Gio.NotificationPriority.HIGH)
        print(Gio.Application.get_default(), n)
        self.notifications[nid] = n
        Gio.Application.get_default().send_notification(nid, n)
    """

    def update_back_button(self):
        inleaflet = self.mainstack.get_visible_child() == self.leaflet
        if inleaflet:
            folded = self.leaflet.get_property('folded')
            firstfold = self.leaflet.get_visible_child() == self.main_menu
            show = (folded and not firstfold) or self.stack_count > 1
        else:
            show = False
        self.back_button.set_visible(show)

    def on_stack_changed(self, *args):
        if self.stack.get_transition_running():
            # wait for transition end
            return
        stack_children = self.stack.get_children()
        lastid = len(stack_children) - 1
        currentid = stack_children.index(self.stack.get_visible_child())
        if currentid == (lastid-1):
            # we got back one page, destroy last page
            stack_children[lastid].destroy()
            self.stack_count = self.stack_count - 1
        self.update_back_button()

    def goBack(self, *args):
        if (self.stack_count > 1):
            self.stack.navigate(Handy.NavigationDirection.BACK)
            """
            #self.stack.set_transition_type(Gtk.StackTransitionType.SLIDE_RIGHT)
            current = self.stack.get_children()[-1]
            previous = self.stack.get_children()[-2]
            self.stack.set_visible_child(previous)
            self.stack_count = self.stack_count - 1
            
            folded = self.leaflet.get_property('folded')
            while not folded and self.leaflet.get_child_transition_running():
                pass
            current.destroy()
            """
        else:
            self.leaflet.set_visible_child(self.main_menu)
        self.update_back_button()

    def pushPage(self, page):
        self.mainstack.set_visible_child(self.leaflet)
        page._set_pagestack(self)
        self.stack.add(page)
        self.stack_count = self.stack_count + 1
        page.show_all()
        self.leaflet.set_visible_child(self.stack)
        self.stack.set_visible_child(page)

        self.update_back_button()

    def replaceWithPage(self, page):
        self.mainstack.set_visible_child(self.leaflet)
        page._set_pagestack(self)
        self.stack.add(page)
        page.show_all()
        self.leaflet.set_visible_child(self.stack)
        self.stack.set_visible_child(page)
        self.stack_count = 1
        
        #folded = self.leaflet.get_property('folded')
        #while not folded and self.leaflet.get_child_transition_running():
        #    pass
        
        for w in self.stack.get_children()[:-1]:
            w.destroy()

        self.update_back_button()


class ConfyWindow(Gtk.ApplicationWindow):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.set_icon_name(APP_ID)
        self.set_default_size(800,600)

        self.overlay = Gtk.Overlay.new()
        self.add(self.overlay)

        self.notification = NotificationOverlay()
        self.overlay.add_overlay(self.notification)

        self.loading = LoadingOverlay()
        self.overlay.add_overlay(self.loading)

        # win actions
        edit_meta_action = Gio.SimpleAction.new("edit-meta", None)
        edit_meta_action.connect("activate", self.edit_meta)
        self.add_action(edit_meta_action)

        return_to_events_action = Gio.SimpleAction.new("return-to-events", None)
        return_to_events_action.connect("activate", lambda *_: self.show_start() )
        self.add_action(return_to_events_action)

        show_preferences_action = Gio.SimpleAction.new("show-preferences", None)
        show_preferences_action.connect("activate", self.show_preferences )
        self.add_action(show_preferences_action)

        about_action = Gio.SimpleAction.new("about", None)
        about_action.connect("activate", self.show_about)
        self.add_action(about_action)

        self.view = None
        self.show_all()
        
        self.show_start()

    def show_preferences(self, *args):
        pref = PreferencesWindow(transient_for=self)
        pref.show_all()

    def edit_meta(self, *args):
        if isinstance(self.view, MainView):
            self.view.edit_meta()

    def show_about(self, *args):
        Gtk.AboutDialog(
            program_name=APP_NAME,
            logo_icon_name=APP_ID,
            version=APP_VERSION,
            copyright="© 2020 Fabio Comuni",
            website="https://confy.kirgroup.net/",
            authors=["Fabio Comuni"],
            comments=_("Conference schedules viewer"),
            license_type=Gtk.License.GPL_3_0
        ).show()

    def show_start(self):
        """start page, list conferences"""
        if self.view is not None:
            self.view.destroy()
        self.view = StartView(self)
        self.overlay.add(self.view)
        local.close()
    
    def show_main(self, conf):
        """start page, list conferences"""
        if self.view is not None:
            self.view.destroy()
        self.view = MainView(self, conf)
        self.overlay.add(self.view)


